[![Build Status](https://travis-ci.org/jhipster/generator-jhipster.svg?branch=master)](https://travis-ci.org/jhipster/generator-jhipster)

Full documentation and information is available on our website at [http://jhipster.github.io/](http://jhipster.github.io/)


# 使用方法




* 首先安装nodejs运行环境
* 下载项目安装到本地

```
git clone http://git.oschina.net/big.boot/generator-jhipster ./generator-jhipster
cd generator-jhipster 
npm install -g yo
npm install
```

* 运行生成器

```
yo jhipster
```

输入相关需要生成的信息，自动生成项目并进行构建

* 运行生成出来的项目

```
cd jhipster-sample-app
mvn spring-boot:run
```


项目使用到的技术

* 单页式应用
* 响应式自适应页面设计，基于TWITTTER BOOTSTRAP
* AngularJS 来自GOOGLE的JS技术
* 国际化支持
* 可选的CSS前端开发压缩SASS支持
* 基于Atmosphere的WebSocket支持


# 运行后项目截图

<img src="http://jhipster.github.io/images/screenshot_1.png"/>
<img src="http://jhipster.github.io/images/screenshot_2.png"/>
<img src="http://jhipster.github.io/images/screenshot_3.png"/>
<img src="http://jhipster.github.io/images/screenshot_4.png"/>
<img src="http://jhipster.github.io/images/screenshot_5.png"/>
